<?php
namespace Tempel;

use Exception;
use ReflectionClass;

class Component implements IComponent {

	private static $cache = array();
	private static $pluginContainer = array();
	private static $cacheOptions = array(
		'fileCache' => false,
		'cacheDir' => NULL,
		'cacheFile' => NULL,
		'cacheLoaded' => false
	);

	public static function EnableFileCache($enable=true) {
		Component::$cacheOptions['fileCache'] = $enable;
	}

	public static function SetCacheDir($path) {
		if (substr($path, -1) !== '/') {
			$path = $path . '/';
		}
		if (!is_dir($path)) {
			mkdir($path);
		}
		$cacheFile = $path . 'components';
		Component::$cacheOptions['cacheDir'] = $path;
		Component::$cacheOptions['cacheFile'] = $cacheFile;
	}

	public static function RegisterPlugin($plugin) {
		if ($plugin instanceof IComponentPlugin) {
			Component::$pluginContainer[$plugin->getIdentifier()] = $plugin;
			return true;
		}
		return false;
	}

	private static function Cache() {
		if (Component::$cacheOptions['fileCache']) {
			$cacheObject = serialize(Component::$cache);
			file_put_contents(Component::$cacheOptions['cacheFile'], $cacheObject);
		}
	}

	private static function LoadCache() {
		if (Component::$cacheOptions['fileCache'] && !Component::$cacheOptions['cacheLoaded']) {
			$cacheFile = Component::$cacheOptions['cacheFile'];
			if (file_exists($cacheFile)) {
				$cacheObject = file_get_contents($cacheFile);
				$cache = unserialize($cacheObject);
				Component::$cache = $cache;
			}
			Component::$cacheOptions['cacheLoaded'] = true;
		}
	}



	private $valueMap = array();
	private $options = array(
		'cache' => true,
		'templateDir' => 'html',
		'classDir' => 'php'
	);

	private function resolve($key) {
		if (substr($key, 0, 1) === '@') {
			$key = substr($key, 1);
			if (isset($this->valueMap[$key]))
				return $this->valueMap[$key];
		}
		return $key;
	}

	public function parse() {
		Component::LoadCache();

		$templateName = get_Class($this);

		$matchCount = NULL;
		$matches = NULL;
		$content = NULL;

		$explicitCache = false;

		if (!isset(Component::$cache[$templateName])) {
			$componentBasePath = str_replace("\\", "/", $this->getDir());

			$directories = explode('/', $componentBasePath);

			for ($i = sizeof($directories) - 1; $i >= 0; $i--) {
				if ($directories[$i] === $this->options['classDir']) {
					$directories[$i] = $this->options['templateDir'];
				}
			}
			$componentBasePath = join('/', $directories);

			$filePath = join('/', array(
				$componentBasePath,
				$templateName . '.html'));


			if (file_exists($filePath)) {
				$tmpContent = file_get_contents($filePath);

				$pattern = "/\\$\\{(?:([@=])|([^:^}]+):)?([^}]+)\\}/mi"; 
				//$pattern = "/\\$\\{(?:([@=])|([^:^}]+):)?([^}]*)\\}/mi";
				//$pattern = "/\\$\\{([^}:@=]+)\\}/mi"; 

				$matchCount = preg_match_all($pattern, $tmpContent, $matches, PREG_SET_ORDER);
				$content = $tmpContent;
			} else {
				throw new Exception('Template file ' . $filePath . ' does not exist');
			}
			$explicitCache = $this->options['cache'];
		} else {
			$matchCount = Component::$cache[$templateName]['matchCount'];
			$matches = Component::$cache[$templateName]['matches'];
			$content = Component::$cache[$templateName]['content'];
		}

		if ($explicitCache === true) {
			Component::$cache[$templateName] = array(
				'content' => $content,
				'matches' => $matches,
				'matchCount' => $matchCount
			);

			Component::Cache();
		}

		if ($matchCount !== FALSE && $matchCount > 0) {
			for ($i = 0; $i < $matchCount; $i++) {
				$match = $matches[$i];
				$matchSize = sizeof($match);

				$replaceValue = '';

				if ($matchSize === 4) {
					$key = $this->resolve($match[3]);
					if ($match[1] !== '' && $match[1] !== '@') {
						// TODO: Implement special cases if needed
					} else if ($match[2] !== '') {
						$pluginName = $match[2];
						if (isset(Component::$pluginContainer[$pluginName])) {
							$replaceValue = Component::$pluginContainer[$pluginName]->call($key, $this);
						}
					} else {
						if (isset($this->valueMap[$key])) {
							$replaceValue = $this->valueMap[$key];
							if ($replaceValue instanceof IComponent) {
								$replaceValue = $replaceValue->parse();
							}
						}
					}
				}
				$content = str_replace($match[0], $replaceValue, $content);
			}
		}

		return $content;
	}

	public function setValue($key, $value) {
		return ($this->valueMap[$key] = $value);
	}

	public function setOption($key, $value) {
		$this->options[$key] = $value;
	}

	public function render() {
		echo $this->parse();
	}

	private function getDir() {
		$reflector = new ReflectionClass(get_class($this));
		return dirname($reflector->getFileName());
        //return dirname((new ReflectionClass(static::class))->getFileName()); // Working on php 5.5+ !faster!
	}
}

?>