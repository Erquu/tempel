<?php
namespace Tempel;

interface IComponent {
	function setValue($key, $value);
	function setOption($key, $value);
	function parse();
	function render();
}

?>