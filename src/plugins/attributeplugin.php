<?php
namespace Tempel\Plugins;

use Tempel\IComponentPlugin;

class AttributePlugin implements IComponentPlugin {
	public function getIdentifier() {
		return 'attr';
	}

	public function call($list, $component) {
		$attributeString = '';
		if (is_array($list)) {
			foreach ($list as $key => $value) {
				if (is_int($key)) {
					$attributeString .= ' ' . $value;
				} else {
					$attributeString .= ' ' . $key . '="' . $value . '"';
				}
			}
		}
		return $attributeString;
	}
}

?>