<?php
namespace Tempel\Plugins;

use Tempel\IComponentPlugin;

class TestPlugin implements IComponentPlugin {
	public function getIdentifier() {
		return 'default';
	}

	public function call($key, $component) {
		return $key . $key;
	}
}

?>