<?php

use Tempel\Component;

abstract class TemplateList extends Component {

	protected $components = array();
	protected $placeholder = NULL;

	public function __construct($placeholder) {
		$this->placeholder = $placeholder;
	}

	public function clearComponents() {
		$this->components = array();
	}

	public function addComponent($component) {
		array_push($this->components, $component);
		return $component;
	}

	public function parse() {
		$content = '';
		for ($i = 0; $i < sizeof($this->components); $i++) {
			$content .= $this->components[$i]->parse();
		}
		$this->setValue($this->placeholder, $content);
		return parent::parse();
	}

	public function render() {
		echo $this->parse();
	}
}

?>