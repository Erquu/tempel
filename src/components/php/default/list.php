<?php
use Tempel\TemplatelessComponent;

class ComponentList extends TemplatelessComponent {

	protected $components = array();

	public function addComponent($component) {
		array_push($this->components, $component);
	}

	public function parse() {
		$content = '';
		for ($i = 0; $i < sizeof($this->components); $i++) {
			$content .= $this->components[$i]->parse();
		}
		return $content;
	}
}

?>