<?php
use Tempel\TemplatelessComponent;

class StaticComponent extends TemplatelessComponent {

	protected $content = '';

	public function __construct($content) {
		$this->content = (string)$content;
	}

	public function parse() {
		return $this->content;
	}
}

?>