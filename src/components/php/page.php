<?php
use Tempel\IComponent;
use Tempel\Component;

class Page extends Component {

	private $head = NULL;

	public function __construct($title, $content=NULL) {
		$this->head = new ComponentList();

		$this->setValue('title', $title);
		$this->setValue('content', $content);
		$this->setValue('head', $this->head);
	}

	public function addHead($c) {
		if (!($c instanceof IComponent)) {
			$c = new StaticComponent($c);
		}
		$this->head->addComponent($c);
		return $c;
	}

	public function setContent($content) {
		$this->setValue('content', $content);
	}
}

?>