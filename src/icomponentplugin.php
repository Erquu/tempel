<?php
namespace Tempel;

interface IComponentPlugin {
	function getIdentifier();
	function call($key, $component);
}

?>