<?php

/**
 * Include the default "Tempel" libraries
 */
require_once __DIR__ . '/icomponent.php';
require_once __DIR__ . '/icomponentplugin.php';

require_once __DIR__ . '/component.php';
require_once __DIR__ . '/templatelesscomponent.php';

require_once __DIR__ . '/plugins/testplugin.php';
require_once __DIR__ . '/plugins/attributeplugin.php';

require_once __DIR__ . '/components/php/page.php';
require_once __DIR__ . '/components/php/default/list.php';
require_once __DIR__ . '/components/php/default/templatelist.php';
require_once __DIR__ . '/components/php/default/staticcomponent.php';

/**
 * Register default Component Plugins
 */
Tempel\Component::RegisterPlugin(new Tempel\Plugins\AttributePlugin());

?>