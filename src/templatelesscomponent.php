<?php
namespace Tempel;

use Tempel\IComponent;

class TemplatelessComponent implements IComponent {
	public function setValue($key, $value) { }
	public function setOption($key, $value) { }
	public function parse() {
		return '';
	}
	public function render() {
		echo $this->parse();
	}
}

?>