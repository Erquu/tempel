<?php

	/*for ($i = 0; $i < 10000; $i++) {
		echo '<div><button>Hello World</button></div>';
	}
	return;*/

	require_once '../src/req1.inc.php';

	use TemplateEngine\Component;
	use TemplateEngine\Plugins\TestPlugin;

	require_once 'components/php/test/foo.php';
	require_once 'components/php/button.php';

	Component::RegisterPlugin(new TestPlugin());

	$foo = new Foo(new Button('Hello World'));

	$page = new Ribbon('Test', $foo);
	$page->render();

?>