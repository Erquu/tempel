<?php

use TemplateEngine\Component;

class Foo extends Component {
	
	public function __construct($content) {
		$this->setValue('foo', $content);
	}
}

?>